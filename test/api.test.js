// External libraries
const request = require("supertest");
const jose = require("jose");

// Internal modules
const docs = require("../src/docs.js");
const secrets = require("../src/secrets.js");

// API module
const API = require("../src/api");

describe("The project", () => {
  const api = API({ docs, secrets });

  it("can use Jest", () => {
    expect(true).toBe(true);
  });

  it("can use Supertest", async () => {
    const response = await request(api).get("/");
    expect(response.status).toBe(200);
    expect(response.body).toBe("Hello, World!");
  });

  it("can use CORS", async () => {
    const response = await request(api).get("/");
    const cors_header = response.header["access-control-allow-origin"];
    expect(cors_header).toBe("*");
  });

  it("can use CookieParser", async () => {
    const response = await request(api).get("/");
    const set_cookie_header = response.header["set-cookie"];
    const [_, cookie_name, cookie_value] = set_cookie_header[0].match(
      "(.*)=(.*);"
    );
    expect(cookie_name).toBe("the_answer");
    expect(cookie_value).toBe("42");
  });

  test("can use JOSE", async () => {
    const keystore = new jose.JWKS.KeyStore([
      jose.JWK.asKey("TheAnswerToLifeUniverseAndEverythingElseIs42", {
        kid: "secret",
        alg: "HS256",
        use: "sig",
      }),
    ]);

    const jwt = jose.JWT.sign(
      { msg: "Hello, World!" },
      keystore.get({ kid: "secret" }),
      {
        subject: "test",
        issuer: "test",
        audience: "JOSE is working",
        algorithm: "HS256",
      }
    );

    const payload = jose.JWT.verify(jwt, keystore, {
      subject: "test",
      issuer: "test",
      audience: "JOSE is working",
      algorithms: ["HS256"],
    });

    expect(payload.msg).toBe("Hello, World!");
  });
});
