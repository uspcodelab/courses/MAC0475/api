// Node libraries
const fs = require("fs");

// External libraries
const request = require("supertest");

// Internal modules
const secrets = require("../../src/secrets.js");
const docs = require("../../src/docs.js");

// API module
const API = require("../../src/api.js");

// Environment variables
const API_SECRETS_PATH =
  process.env.API_SECRETS_PATH || "./.secrets/development";

describe("When secrets are available in the environment, the API", () => {
  const api = API({ docs, secrets });

  // Skip integration tests if secrets are unavailable
  const secretsExist = fs.existsSync(API_SECRETS_PATH);
  const whenSecrets = secretsExist ? it : it.skip;

  whenSecrets("can sign cookies with a symetric key", async () => {
    const response = await request(api).get("/");

    const set_cookie_header = response.header["set-cookie"];
    const capture = set_cookie_header[1].match("(.*)=s%3A(.*)[.](.*);");
    const [_, cookie_name, cookie_value, cookie_signature] = capture;

    expect(cookie_name).toBe("ramanujan");
    expect(cookie_value).toBe("1729");
    expect(cookie_signature).toBe(
      "FQf0NrdgAhlnZ4Z%2B6hNbYpiNB8p55Nau9kAr1OdPEiQ"
    );
  });
});
